﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Upload
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UploadFiles" in both code and config file together.
    public class UploadFiles : IUploadFiles
    {
        public string login(string username, string password)
        {
            if (string.IsNullOrEmpty(username) && string.IsNullOrEmpty(password))
                return "username and password are  required";
            if (string.IsNullOrEmpty(username))
                return "username is required";
            if (string.IsNullOrEmpty(password))
                return "password is required";

            try
            {
                var query = @"SELECT* FROM users where username = @username
                and password = SHA2(@password, 256)";
                string connStr = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (MySqlConnection cnn = new MySqlConnection(connStr))
                using (MySqlCommand cmd = new MySqlCommand(query, cnn))
                {
                    cnn.Open();
                    cmd.Parameters.Add("@username", MySqlDbType.VarChar).Value = username;
                    cmd.Parameters.Add("@password", MySqlDbType.VarChar).Value = password;
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add("users");
                        ds.Tables[0].Load(reader);
                        string data = ds2json(ds);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            string id = ds.Tables[0].Rows[0]["id"].ToString();
                            string name = ds.Tables[0].Rows[0]["username"].ToString();
                            string UserToken = OperationContext.Current.SessionId;
                            return $"login,{id},{name}";

                        }
                        return "wrong username or password";
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static string ds2json(DataSet ds)
        {
            return JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.Indented);
        }

        public static bool ParseCreateFile(List<String> lines, string path)
        {
            try
            {
                string[] row1 = lines[0].Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                string FUN = row1[1].Substring(0, 1);
                DateTime CreationTime = DateTime.ParseExact(row1[1].Substring(row1[1].Length - 8), "yyyyMMdd", CultureInfo.InvariantCulture);
                string[] last = lines.Last().Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                string TotalDebitCount = last[1];
                string TotalDebitAmount = last[2];
                string TotalCreditCount = last[3];
                string TotalCreditAmount = last[4];
                string FileName = path.Split('\\').Last();
                string PhysicalLocation = path;

                var query = @"INSERT INTO inputfinancialfile
                            (FUN, CreationTime, TotalCreditCount, TotalCreditAmount, TotalDebitCount, TotalDebitAmount, FileName, PhysicalLocation)
                            VALUES(@FUN,@CreationTime,@TotalCreditCount,@TotalCreditAmount,@TotalDebitCount,@TotalDebitAmount,@FileName,@PhysicalLocation);
                          ";
                string connStr = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (MySqlConnection cnn = new MySqlConnection(connStr))
                using (MySqlCommand cmd = new MySqlCommand(query, cnn))
                {
                    cnn.Open();
                    cmd.Parameters.Add("@FUN", MySqlDbType.VarChar).Value = FUN;
                    cmd.Parameters.Add("@CreationTime", MySqlDbType.VarChar).Value = CreationTime.Date.ToString("yyyy-MM-dd");
                    cmd.Parameters.Add("@TotalCreditCount", MySqlDbType.VarChar).Value = TotalCreditCount;
                    cmd.Parameters.Add("@TotalCreditAmount", MySqlDbType.VarChar).Value = TotalCreditAmount;
                    cmd.Parameters.Add("@TotalDebitCount", MySqlDbType.VarChar).Value = TotalDebitCount;
                    cmd.Parameters.Add("@TotalDebitAmount", MySqlDbType.VarChar).Value = TotalDebitAmount;
                    cmd.Parameters.Add("@FileName", MySqlDbType.VarChar).Value = FileName;
                    cmd.Parameters.Add("@PhysicalLocation", MySqlDbType.VarChar).Value = PhysicalLocation;
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.RecordsAffected > 0)
                            return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                string error = ex.Message.ToString();
                return false;
            }
        }

        public static bool mergeFiles(List<List<String>> Files)
        {
            string path = $@"C:\Users\amr\Desktop\output{DateTime.Now.Ticks}.txt";
            string pdfpath = $@"C:\Users\amr\Desktop\output{DateTime.Now.Ticks}.pdf";
            List<string> list = new List<string>();
            int DC = 0;
            int DA = 0;
            int CC = 0;
            int CA = 0;

            string lastLine = "";
            string[] row1 = Files[0][0].Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            list.Add($"FH      {row1[1]}");
            for (var i = 0; i < Files.Count; i++)
            {
                for (var j = 1; j < Files[i].Count; j++)
                {
                    string[] row = Files[i][j].Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    if (row[0] == "FR")
                    {
                        string BankTransAmount = $"FR      {j}      {row[2]}      {row[3]}";
                        int index = list.FindIndex(str => str.Contains(row[2]));

                        if (index == -1)
                        {
                            list.Add(BankTransAmount);
                        }
                        else
                        {
                            string[] line = list[index].Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                            int sum = Convert.ToInt32(line[3]) + Convert.ToInt32(row[3]);
                            BankTransAmount = $"FR      {j}      {row[2]}      {sum}";
                            list[index] = BankTransAmount;
                        }
                    }
                    if (row[0] == "FT")
                    {

                        DC = Convert.ToInt32(row[1]) + DC;
                        DA = Convert.ToInt32(row[2]) + DA;
                        CC = Convert.ToInt32(row[3]) + CC;
                        CA = Convert.ToInt32(row[4]) + CA;
                        lastLine = $"FT      {DC}      {DA}      {CC}      {CA} ";
                    }

                }

            }
            list.Add(lastLine);
            string[] lines = list.ToArray();
            File.WriteAllLines(path, lines);
            try
            {
                string[] newRow1 = lines[0].Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                string FUN = newRow1[1].Substring(0, 1);
                DateTime CreationTime = DateTime.ParseExact(newRow1[1].Substring(newRow1[1].Length - 8), "yyyyMMdd", CultureInfo.InvariantCulture);
                string TotalDebitCount = DC.ToString();
                string TotalDebitAmount = DA.ToString();
                string TotalCreditCount = CC.ToString();
                string TotalCreditAmount = CA.ToString();
                string PhysicalLocation = path;

                var query = @"INSERT INTO outputfinancialfile
                            (FUN, CreationTime, TotalCreditCount, TotalCreditAmount, TotalDebitCount, TotalDebitAmount, PhysicalLocation)
                            VALUES(@FUN,@CreationTime,@TotalCreditCount,@TotalCreditAmount,@TotalDebitCount,@TotalDebitAmount,@PhysicalLocation);
                          ";
                string connStr = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (MySqlConnection cnn = new MySqlConnection(connStr))
                using (MySqlCommand cmd = new MySqlCommand(query, cnn))
                {
                    cnn.Open();
                    cmd.Parameters.Add("@FUN", MySqlDbType.VarChar).Value = FUN;
                    cmd.Parameters.Add("@CreationTime", MySqlDbType.VarChar).Value = CreationTime.Date.ToString("yyyy-MM-dd");
                    cmd.Parameters.Add("@TotalCreditCount", MySqlDbType.VarChar).Value = TotalCreditCount;
                    cmd.Parameters.Add("@TotalCreditAmount", MySqlDbType.VarChar).Value = TotalCreditAmount;
                    cmd.Parameters.Add("@TotalDebitCount", MySqlDbType.VarChar).Value = TotalDebitCount;
                    cmd.Parameters.Add("@TotalDebitAmount", MySqlDbType.VarChar).Value = TotalDebitAmount;
                    cmd.Parameters.Add("@PhysicalLocation", MySqlDbType.VarChar).Value = PhysicalLocation;
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.RecordsAffected > 0)
                            return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                string error = ex.Message.ToString();
                return false;
            }
        }

        public string addFiles(List<byte[]> files, string username)
        {

            try
            {
                string path;
                bool valid;
                List<List<String>> Files = new List<List<string>>();
                foreach (var fileBytes in files)
                {
                    using (var file = new MemoryStream(fileBytes))
                    {
                        using (var fs = File.Open($@"C:\Users\amr\Desktop\input{DateTime.Now.Ticks}.txt", FileMode.OpenOrCreate))
                        {
                            file.CopyTo(fs);
                            path = fs.Name;
                        }
                        List<String> lines = File.ReadLines(path).ToList();
                        Files.Add(lines);
                        valid = ParseCreateFile(lines, path);
                        if (!valid)
                            return null;
                        return valid.ToString();
                    }
                }
                valid = mergeFiles(Files);
                if (!valid)
                    return null;
                return valid.ToString();
                //create batch file 

            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        public DataSet GetFiles(string username)
        {
            try
            {
                var query = @"SELECT* FROM outputfinancialfile";
                string connStr = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                SqlDataAdapter da = new SqlDataAdapter(query, connStr);
                // Create a New DataSet
                DataSet ds = new DataSet();
                // Fill The DataSet With the Contents of the Stock Table
                da.Fill(ds, "Files");
                return (ds);
            }
            catch (Exception ex)
            {
                var error = ex.Message.ToString();
                return null;
            }
        }
    }
}