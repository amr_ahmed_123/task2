﻿using System.ServiceModel;

namespace Upload
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUploadFiles" in both code and config file together.
    [ServiceContract]
    public interface IUploadFiles
    {
        [OperationContract]
        string login(string username, string password);

        [OperationContract]
        string addFiles(List<byte[]> files, string username);

        [OperationContract]
        DataSet GetFiles(string username);
    }
}