﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceReference1;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;

namespace FileUpload.Controllers
{
    public class uploadFileController : Controller
    {
        // GET: uploadFile
        public ActionResult uploadFile(string userid, string name)
        {
            return View(new { name, userid });
        }
        public async Task<ActionResult> viewFiles()
        {
            Service1Client service = new Service1Client();
            string[] keys = { "view", "1", "amr" };
            var files = await service.GetFilesAsync(keys[2]);
            ViewData["files"] = files;
            return View(ViewData);
        }
        [HttpPost]
        public async Task<ActionResult> upload(List<IFormFile> files)
        {
            string[] keys = { "upload", "1", "amr" };
            if (files.Count == 0)
                return RedirectToAction("uploadFile", "uploadFile", new { name = keys[2], userid = keys[1] });
            Service1Client service = new Service1Client();
            List<byte[]> fileslist = new List<byte[]>();
            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    using (var ms = new MemoryStream())
                    {
                        formFile.CopyTo(ms);
                        byte[] fileBytes = ms.ToArray();
                        fileslist.Add(fileBytes);
                    }
                }
            }
            var x = await service.addFilesAsync(fileslist.ToArray(), keys[2]);
            return RedirectToAction("uploadFile", "uploadFile", new { name = keys[2], userid = keys[1] });
        }


    }
}