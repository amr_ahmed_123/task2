﻿using FileUpload.Models;
using Microsoft.AspNetCore.Mvc;
using ServiceReference1;
using System.Diagnostics;
using System.Threading.Tasks;

namespace FileUpload.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(userLogin userLogin)
        {
            var user = userLogin;

            if (ModelState.IsValid)
            {
                Service1Client serv = new Service1Client();
                var message = await serv.loginAsync(user.Name, user.Password);
                string[] keys = message.loginResult.Split(',');
                if ("login" == keys[0])
                    return RedirectToAction("uploadFile", "uploadFile", new { name = keys[2], userid = keys[1] });
            }

            return View(userLogin);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}