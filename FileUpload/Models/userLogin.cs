using System.ComponentModel.DataAnnotations;

namespace FileUpload.Models
{
    public class userLogin
    {
        [Required]
        [StringLength(100, ErrorMessage = "Your name is required")]
        public string Name { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Your name is required")]
        public string Password { get; set; }
    }
}